import sys
import urllib.request
import json

# adres do stacji w Warszawie i odczyt otwarcie linku za pomocą urllib
url = 'https://danepubliczne.imgw.pl/api/data/synop/station/' + str.lower(sys.argv[1])
req = urllib.request.urlopen(url).read().decode('utf-8')
js = json.loads(req)

print("Miasto: {}".format(js['stacja']))
print("Data pomiaru: {}".format(js['data_pomiaru']))
print("Temp: {}".format(js['temperatura']))
print("Cisnienie: {}".format(js['cisnienie']))